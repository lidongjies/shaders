precision highp float;

void main() {
    vec2 uv = gl_FragCoord.xy / iResolution.xy;
    vec2 center = vec2(0.5, 0.5);
    float d = distance(uv, center);
    gl_FragColor.rgb = smoothstep(d, d + 0.001, 0.3) * vec3(1.0);
    gl_FragColor.a = 1.0;
}